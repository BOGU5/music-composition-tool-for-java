
/**
 * A virtual musical note and some methods with which to compose.
 *
 * @author Zach McDaniel
 * @version 2019/5/13
 */
class Note {
    int pitch;
    int octave;
    int voice = 1;
    double rhythmicValue;
    double iTime;
    String letterValue;
    /**
     * Default constructor, a quarter-note A4 
     */
    public Note() {
        this(1, 4, 1.0);
    }

    /**
     * Constructs a Note object with a pitch and rhythmic value.
     * 
     * @param pch integer between 1 and 12 representing a note value
     * @param rtm double representing note duration
     */
    public Note(int pch, double rtm) {
        int nthOctave = (int)(pch/12);
        pitch = pch % 12;
        if(pitch == 0)
            pitch = 12;    
        octave = nthOctave + 1;
        rhythmicValue = rtm;
    }

    /**
     * Constructs a Note object with a pitch and octave.
     * 
     * @param pch integer between 1 and 12 representing a note value
     * @param oct integer representing octave value
     */
    public Note(int pch, int oct) {
        pitch = pch % 12;
        if(pitch == 0)
            pitch = 12;
        octave = oct;
    }

    /**
     * Constructor for just setting a pitch.
     * 
     * @param pch An integer between 1 and 12
     */
    public Note(int pch) {
        pitch = pch % 12;
        if (pitch == 0)
            pitch = 12;
    }

    /**
     * Constructor for manually setting all note variables.
     * 
     * @param pch An integer between 1 and 12 representing pitch
     * @param oct An integer representing octave
     * @param rtm A double representing duration
     */
    public Note(int pch, int oct, double rtm) {
        pitch = pch % 12;
        if(pitch == 0)
            pitch = 12;
        //add if statement for if pch > 12 add octave(s)
        octave = oct;
        rhythmicValue = rtm;
    }

    /**
     * Sets the rhythmic value of Note object.
     * 
     * @param rtm A double representing duration
     */
    public void setRhythmicValue(double rtm) {
        rhythmicValue = rtm;
    }

    /**
     * Sets the octave value of Note object.
     * 
     * @param oct An integer representing octave value
     */
    public void setOctave(int oct) {
        octave = oct;
    }

    /**
     * Gets the String value of a note's pitch.
     * 
     * @return String, letter value associated with note pitch.
     */
    public String getLetterValue() {
        String[] letters = {"A", "A-Sharp/B-Flat", "B/C-Flat", "B-Sharp/C",
                            "C-Sharp/D-Flat", "D", "D-Sharp/E-Flat",
                            "E/F-Flat", "E-Sharp/F", "F-Sharp/G-Flat",
                            "G", "G-Sharp/A-Flat"};
        return letters[pitch - 1];
    }

    /**
     * Creates a new note object n semi-tones from the note on which it
     * is called.
     * 
     * @param n An integer representing a number of semi-tones
     * 
     * @return A new note object
     */
    public Note nSemiTonesFrom(int n) {
        int oct = this.octave;
        int pch = this.pitch + n;
        if(pch > 12)
            oct += (int)(pch/12);
        Note note = new Note(pch, oct);
        return note;
    }

    /**
     * Determines number of semi-tones between this object and given Note
     * object
     * 
     * @param note A Note object
     * 
     * @return integer difference between two pitches
     */
    public int interval(Note note){
        int interval = note.pitch - this.pitch;
        if(interval < 0)
            interval += 12;
        return interval;
    }

    /**
     * Determines the abbreviation for a given interval (From unison to
     * two octaves above, 0 - 24 inclusive).
     * 
     * @param interval An integer representing number of semi-tones
     * 
     * @ return A string of the corresponding abbreviation
     */
    public static String intervalName(int interval){
        String[] names = {"P1/d2 (unison)", "m2/A1", "M2/d3", "m3/A2", 
                          "M3/d4", "P4/A3", "d5/A4 (tritone)" ,"P5/d6",
                          "m6/A5", "M6/d7", "m7/A6", "M7/d8", 
                          "P8/A7 (octave)",
                          "m9/A8", "M9/d10", "m10/A9", "M10/d11", "P11/A10", 
                          "d12/A11" ,"P12/d13", "m13/A12", "M13/d14",
                          "m14/A13", "M14/d15", "P15/A14 (2 octaves)"};
        String name = names[interval];
        return name;
    }

    /**
     * Get rhythmic value of note.
     * 
     * @return double, note's rhythmic value
     */
    public double getRhythmicValue() {
        return rhythmicValue;
    }

    /**
     * Prints message giving note's name, octave and duration.
     */
    public void displayNoteDetails() {
        System.out.println(this.getLetterValue() + ", Octave: " + this.octave
                           + ", iTime: " + iTime + ", Duration: " + this.getRhythmicValue());
    }

    /**
     * Note object's major scale.
     * 
     * @param rtm double, the duration of each note
     * 
     * @return A scale object
     */
    public Scale majorScale(double rtm) {
        int[] scaleIntervals = {0, 2, 4, 5, 7, 9, 11};
        return new Scale((this.getLetterValue() + " Major Scale"), 
            this, scaleIntervals, rtm);
    }

    /**
     * Note object's melodic minor scale.
     * 
     * @param rtm double, the duration of each note
     * 
     * @return A scale object
     */
    public Scale melodicMinorScale(double rtm) {
        int[] scaleIntervals = {0, 2, 3, 5, 7, 9, 11};
        return new Scale((this.getLetterValue() + " Melodic Minor Scale"), 
            this, scaleIntervals, rtm);
    }

    /**
     * Note object's natural minor scale.
     * 
     * @param rtm double, the duration of each note
     * 
     * @return A scale object
     */
    public Scale naturalMinorScale(double rtm) {
        int[] scaleIntervals = {0, 2, 3, 5, 7, 8, 10};
        return new Scale((this.getLetterValue() + " Natural Minor Scale"), 
            this, scaleIntervals, rtm);
    }

    /**
     * Note object's harmonic minor scale.
     * 
     * @param rtm double, the duration of each note
     * 
     * @return A scale object
     */
    public Scale harmonicMinorScale(double rtm) {
        int[] scaleIntervals = {0, 2, 3, 5, 7, 8, 11};
        return new Scale((this.getLetterValue() + " Harmonic Minor Scale"), 
            this, scaleIntervals, rtm);
    }

    /**
     * Note object's major triad.
     * 
     * @return A Chord object
     */
    public Chord majorTriad() {
        int[] chordIntervals = {0, 4, 7};
        return new Chord((this.getLetterValue() + " Major Triad"), this, 
            chordIntervals);
    }
    
    /**
     * Note object's major triad, in optional inversion
     * 
     * @param inversion integer 1 for first, 2 for second inversion
     * 
     * @return a Chord object
     */
    public Chord majorTriad(int inversion) {
        int semiTonesFrom = 0;
        if(inversion == 1)
            semiTonesFrom = 4;
        else if(inversion == 2)
            semiTonesFrom = 7;
        int[][] inversions = new int[][] {{0, 4, 7}, {0, 3, 8}, {0, 5, 9}};
        return new Chord((this.getLetterValue() + " Major Triad " + inversion
                          + " Inversion"), this.nSemiTonesFrom(semiTonesFrom),
                          inversions[inversion]);
    }

    /**
     * Note object's minor triad
     * 
     * @return a Chord object
     */
    public Chord minorTriad() {
        int[] chordIntervals = {0, 3, 7};
        return new Chord((this.getLetterValue() + " Minor Triad"), this,
            chordIntervals);
    }
    
    /**
     * Note object's minor triad, in optional inversion
     * 
     * @param inversion integer 1 for first, 2 for second inversion
     * 
     * @return a Chord object
     */
    public Chord minorTriad(int inversion) {
        int semiTonesFrom = 0;
        if(inversion == 1)
            semiTonesFrom = 3;
        else if(inversion == 2)
            semiTonesFrom = 7;
        int[][] inversions = new int[][] {{0, 3, 7}, {0, 4, 9}, {0, 5, 8}};
        return new Chord((this.getLetterValue() + " Minor Triad " + inversion
                          + " Inversion"), this.nSemiTonesFrom(semiTonesFrom),
                          inversions[inversion]);
    }
    
    /**
     * Note object's diminished triad.
     * 
     * @return A Chord object
     */
    public Chord diminishedTriad() {
        int[] chordIntervals = {0, 3, 6};
        return new Chord((this.getLetterValue() + " Diminished Triad"), this,
            chordIntervals);
    }

    /**
     * Note object's augmented triad.
     * 
     * @return A Chord object
     */
    public Chord augmentedTriad() {
        int[] chordIntervals = {0, 4, 8};
        return new Chord((this.getLetterValue() + " Augmented Triad"), this,
            chordIntervals);
    }

    public double pitchToPCH() {
        return pitchToPCH(this.pitch, this.octave);
    }
    
    public double pitchToPCH(int pitch, int octave) {
        double[] pchs = new double[] {.09, .10, .11, .0, .01, .02, .03,
                                      .04, .05, .06, .07, .08};
        double pch = pchs[pitch - 1];
        double oct = octave + 4.0;
        pch += oct;
        return pch;
    }
}