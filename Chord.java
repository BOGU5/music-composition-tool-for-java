
/**
 * A virtual musical chord with some accompanying methods.
 *
 * @author Zach McDaniel
 * @version 2019/5/26
 */
public class Chord extends Note {
    String name;
    Note[] notes;
    /**
     * Empty constructor for initialization.
     */
    public Chord() {
    }

    /**
     * Construct a Chord object with given array of Note objects
     * 
     * @param notes an Array of Note objects
     */
    public Chord(Note[] notes) {
        this.notes = notes;
    }

    /**
     * Construct a Chord object with given array of Note objects and
     * a duration.
     * 
     * @param rhythmicValue double representing duration
     */
    public Chord(Note[] notes, double rhythmicValue) {
        this(notes);
        chordDuration(rhythmicValue);
    }

    /**
     * Construct a Chord object with two given Note objects and
     * a duration.
     * 
     * @param note1 Note object
     * @param note2 Note object
     * @param rhythmicValue double representing duration
     */
    public Chord(Note note1, Note note2, double rhythmicValue) {
        this(note1, note2);
        chordDuration(rhythmicValue);
    } 

    /**
     * Construct a Chord object with two given Note objects 
     * 
     * @param note1 Note object
     * @param note2 Note object
     */
    public Chord(Note note1, Note note2) {
        notes = new Note[] {note1, note2};
    }

    /**
     * Construct a Chord object with three given Note objects and
     * a duration.
     * 
     * @param note1 Note object
     * @param note2 Note object
     * @param note3 Note object
     * @param rhythmicValue double representing duration
     */
    public Chord(Note note1, Note note2, Note note3, double rhythmicValue) {
        this(note1, note2, note3);
        chordDuration(rhythmicValue);
    }

    /**
     * Construct a Chord object with three given Note objects 
     * 
     * @param note1 Note object
     * @param note2 Note object
     * @param note3 Note object
     */
    public Chord(Note note1, Note note2, Note note3) {
        notes = new Note[] {note1, note2, note3};
    }

    /**
     * Construct a Chord object with given tonal center and array of interval
     * values
     * 
     * @param key Note object
     * @param intervals an Array of integers representing semi-tones from root
     */
    public Chord(Note key, int[] intervals) {
        notes = new Note[intervals.length];
        for(int i = 0; i < intervals.length; ++i) {
            notes[i] = key.nSemiTonesFrom(intervals[i]);
        }
    }

    /**
     * Construct a Chord object with given tonal center and array of interval
     * values
     * 
     * @param key Note object
     * @param intervals an Array of integers representing semi-tones from root
     */
    public Chord(String name, Note key, int[] intervals) {
        this(key, intervals);
        chordName(name);
    }

    /**
     * Sets the Chord object's duration
     * 
     * @param rhythmicValue double representing duration
     */
    public void chordDuration(double rhythmicValue) {
        for(int i = 0; i < notes.length; ++i) {
            notes[i].setRhythmicValue(rhythmicValue);
        }
    }
    
    /**
     * Gives number of notes in Chord
     * 
     * @return an integer, number of notes
     */
    public int chordSize() {
        return notes.length;
    }
    
    /**
     * Displays details of all notes in Chord
     * 
     */
    public void displayChordDetails() {
        for(int i = 0; i < notes.length; ++i) {
            notes[i].displayNoteDetails();
        }
    }
    
    /**
     * Creates a Melody object arpeggiating the Chord object
     * 
     * @param rhythmicValue double representing duration of each note
     * 
     * @return a Melody object, the arpeggiated chord
     */
    public Melody chordArpeggiator(double rhythmicValue) {
        Melody arpeggio = new Melody();
        for(int i = 0; i < this.notes.length; ++i) {
            this.notes[i].rhythmicValue = rhythmicValue;
            arpeggio.addNote(this.notes[i]);
        }
        return arpeggio;
    }
    
    /**
     * Sets Chord object's name
     * 
     * @param name a String
     */
    public void chordName(String name) {
        this.name = name;
    }
}