
/**
 * A simulated chord progression with some accompanying methods.
 *
 * @author Zach McDaniel
 * @version 2019/5/26
 */
public class Progression extends Note {
    Chord[] progression;
    /**
     * Blank constructor for initialization.
     */
    public Progression() {
    }
    
    /**
     * Constructs a Progression object with given Array of Chord objects
     * 
     * @param progression an Array of Chords
     */
    public Progression(Chord[] progression) {
        this.progression = progression; 
    }

    /**
     * Constructs a Progression object with given tonal center, either
     * major or minor tonality, and an array of intervals
     * 
     * @param keyCenter a Note object, the tonic chord
     * @param mode a String, either "major" or "minor"
     * @param relations an Array of integers representing semi-tones from tonic
     */
    public Progression(Note keyCenter, String mode, int[] relations) {
        progression = new Chord[relations.length];
        if(mode.equalsIgnoreCase("major")) {
            for(int i = 0; i < relations.length; ++i) {
                if(majorMajorRules(relations[i]))
                    progression[i] = createTriad(keyCenter.nSemiTonesFrom(relations[i]), 
                        "maj");
                else if(majorMinorRules(relations[i]))
                    progression[i] = createTriad(keyCenter.nSemiTonesFrom(relations[i]), 
                        "min");
                else if(majorDiminishedRules(relations[i]))
                    progression[i] = createTriad(keyCenter.nSemiTonesFrom(relations[i]), 
                        "dim");
            }
        }

        if(mode.equalsIgnoreCase("minor")) {
            for(int i = 0; i < relations.length; ++i) {
                if(minorMajorRules(relations[i]))
                    progression[i] = createTriad(keyCenter.nSemiTonesFrom(relations[i]), 
                        "maj");
                else if(minorMinorRules(relations[i]))
                    progression[i] = createTriad(keyCenter.nSemiTonesFrom(relations[i]), 
                        "min");
                else if(minorDiminishedRules(relations[i]))
                    progression[i] = createTriad(keyCenter.nSemiTonesFrom(relations[i]), 
                        "dim");

            }
        }
    }
    
    /**
     * Checks whether a given tonal relationship in the major mode will be
     * major
     * 
     * @param relation an integer representing semi-tones from tonic
     * 
     * @return boolean
     */
    public boolean majorMajorRules(int relation) {
        return relation == 0 || relation == 5 || relation == 7;
    }

    /**
     * Checks whether a given tonal relationship in the major mode will be
     * minor
     * 
     * @param relation an integer representing semi-tones from tonic
     * 
     * @return boolean
     */
    public boolean majorMinorRules(int relation) {
        return relation == 2 || relation == 4 || relation == 9;
    }

    /**
     * Checks whether a given tonal relationship in the major mode will be
     * diminished
     * 
     * @param relation an integer representing semi-tones from tonic
     * 
     * @return boolean
     */
    public boolean majorDiminishedRules(int relation) {
        return relation == 11;
    }

    /**
     * Checks whether a given tonal relationship in the minor mode will be
     * major
     * 
     * @param relation an integer representing semi-tones from tonic
     * 
     * @return boolean
     */
    public boolean minorMajorRules(int relation) {
        return relation == 3 || relation == 7 || relation == 10;
    }

    /**
     * Checks whether a given tonal relationship in the minor mode will be
     * minor
     * 
     * @param relation an integer representing semi-tones from tonic
     * 
     * @return boolean
     */
    public boolean minorMinorRules(int relation) {
        return relation == 0 || relation == 5;
    }

    /**
     * Checks whether a given tonal relationship in the minor mode will be
     * diminished.
     * 
     * @param relation an integer representing semi-tones from tonic
     * 
     * @return boolean
     */
    public boolean minorDiminishedRules(int relation) {
        return relation == 2 || relation == 11;
    }
    
    /**
     * Displays the note details of all notes of all chords in Progression
     *
     */
    public void displayProgressionDetails() {
        for(int i = 0; i < progression.length; ++i) {
            progression[i].displayChordDetails();
        }
    }
    
    /**
     * Returns the number of chords in Progression
     * 
     * @return an integer, the number of chords
     */
    public int numChords() {
        return progression.length;
    }
    
    /**
     * Returns the size of the biggest chord in Progression
     * 
     * @return an integer, the number of notes in biggest chord
     */
    public int sizeOfBiggestChord() {
        int size = 0;
        for(int i = 0; i < progression.length; ++i) {
            if(progression[i].chordSize() > size)
                size = progression[i].chordSize();
        }
        return size;
    }
    
    /**
     * Given a tonic and a string representing the chord quality, returns
     * a chord object with given tone and quality
     * 
     * @param root a Note object, tonic
     * @param quality "maj", "min", "dim" or "aug" ignoring case
     * 
     * @return a Chord object
     */
    public Chord createTriad(Note root, String quality) {
        Chord chord = new Chord();
        if(quality.equalsIgnoreCase("maj"))
            chord = root.majorTriad();
        else if(quality.equalsIgnoreCase("min"))
            chord = root.minorTriad();
        else if(quality.equalsIgnoreCase("dim"))
            chord = root.diminishedTriad();
        else if(quality.equalsIgnoreCase("aug"))
            chord = root.augmentedTriad();
        return chord;
    }
}
