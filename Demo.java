
/**
 * A simple demonstration of the functionality of my composition engine. 
 * Creates a series of major arpeggios of all 12 keys. First chord is 
 * a C major triad, proceeded by major triad first inversion of the chord
 * a perfect fifth above (G), then the second inversion of the chord a fifth 
 * above that (D), continued in this order (major triad, 1st, 2nd inversion
 * continuing in perfect fifths) through all 12 keys.
 *
 * @author Zach McDaniel
 * @version 2019/5/26
 */
import java.io.*;
public class Demo {
    public static void main(String args[]) 
    throws InterruptedException,IOException {
        double quarterNote = 1.0;
        double dottedQNote = 1.5;
        double eighthNote = 0.5;
        double dottedENote = 0.6666;
        double halfNote = 2.0;
        double wholeNote = 4.0;
        double sixteenthNote = 0.25;
        Note c1 = new Note(4, 4, 0.25);
        Note c2 = new Note(4, 3, 0.75);
        Score cOF = circleOfFifthsArpeggiator(c1, 0.25);
        cOF.nameScore("COF");
        cOF.resetITime();
        cOF.addProgression(circleOfFifthsChords(c2));
        cOF.setBPM(120);
        //cOF.toCSD();
        cOF.play(2);
    }

    /**
     * Creates a score object of major triad arpeggios in zeroth, first
     * and second inversions as the chords cycle through the circle of fifths.
     * 
     * @param firstKey a Note object representing starting key
     * @param rhythmicValue a double representing duration of each note
     * 
     * @return a Score object of arpeggios in all 12 keys
     */
    public static Score circleOfFifthsArpeggiator(Note firstKey,
    double rhythmicValue) {
        Score score = new Score();
        Melody arpeggio = new Melody();
        Progression circleOfFifths = circleOfFifthsChords(firstKey);
        Chord[] progression = circleOfFifths.progression;
        int numKeys = progression.length;
        int inv = 0;
        for(int i = 0; i < numKeys; ++i) {
            if((i + 1) % 2 == 0) {
                progression[i] = progression[i].notes[0].majorTriad(inv);
                for(int j = 0; j < 3; ++j) {
                    progression[i].notes[j] = progression[i].notes[j].nSemiTonesFrom(1);
                }
            }
            arpeggio = progression[i].chordArpeggiator(rhythmicValue);
            inv++;
            inv %= 3;
            for(int j = 0; j < 3; ++j) {
                score.addNote(arpeggio.notes.get(j)); 
            }
        }
        return score;
    }

    /**
     * Creates a Progression object of major triad chords in zeroth, first
     * and second inversions as the chords cycle through the circle of fifths.
     * 
     * @param firstKey a Note object representing starting key
     * 
     * @return a Progression object of major triads in all 12 keys
     */
    public static Progression circleOfFifthsChords(Note firstKey) {
        Progression circleOfFifths = new Progression();
        int inv = 0;
        int numKeys = 12;
        circleOfFifths.progression = new Chord[numKeys];
        Chord[] progression = circleOfFifths.progression;
        for(int i = 0; i < numKeys; ++i) {
            if((i + 1) % 2 == 0)
                progression[i] = rootlessDominantTriad(firstKey.nSemiTonesFrom((i * 7) % 12));
            else
                progression[i] = firstKey.nSemiTonesFrom((i * 7) % 12).majorTriad(inv);
            progression[i].chordDuration(firstKey.rhythmicValue);
            inv++;
            inv %= 3;
        }
        return circleOfFifths;
    }

    public static Chord rootlessDominantTriad(Note note) {
        int[] chordIntervals = {4, 7, 10};
        return new Chord((note.getLetterValue() + " Rootless Dominant Triad"), note, 
            chordIntervals);
    }

}