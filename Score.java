
/**
 * Virtual musical score. Keeps track of various note objects and their
 * data, and the number of voices currently in use.
 *
 * @author Zach McDaniel
 * @version 2019/5/26
 */
import java.util.ArrayList;
import java.util.List;
import java.nio.file.*;
import java.io.*;
import static java.nio.file.StandardOpenOption.*;
import java.lang.ProcessBuilder;
public class Score {
    String name;
    int voices;
    int extantVoices;
    double iTime;
    int bpm;
    List<Note> score = new ArrayList<>();
    /**
     * Default constructor for inititialization.
     */
    public Score() {
    }

    /**
     * Add a Progression object to the score.
     * 
     * @param p Progression object
     */
    public void addProgression(Progression p) {
        Chord[] progression = p.progression;
        setChordITimes(progression);
        for(int i = 0; i < progression.length; ++i) {
            for(int j = 0; j < progression[i].notes.length; ++j) {
                score.add(progression[i].notes[j]);
                progression[i].notes[j].voice = (j + extantVoices) + 1;
            }
        }
        score.add(p);
        int progVoices = p.sizeOfBiggestChord();
        if((progVoices + extantVoices) > voices)
            voices = progVoices;
    }

    /**
     * Add a Melody object to the score.
     * 
     * @param m Melody object
     */
    public void addMelody(Melody m) {
        Note[] melody = m.notes.toArray(new Note[m.notes.size()]);
        setNoteITimes(melody);
        if(voices < 1)
            voices = 1;
        for(int i = 0; i < melody.length; ++i) {
            score.add(melody[i]);
        }
    }

    /**
     * Add a Scale object to the score.
     * 
     * @param s Scale object
     */
    public void addScale(Scale s) {
        setNoteITimes(s.scale);
        score.add(s);
        if(voices < 1)
            voices = 1;
    }

    /**
     * Increases iTime by a given Rest object's duration.
     * 
     * @param r Rest object
     */
    public void addRest(Rest r) {
        iTime += r.rhythmicValue;
    }

    /**
     * Add a single note to the score.
     * 
     * @param note Note object
     */
    public void addNote(Note note) {
        setNoteITime(note);
        score.add(note);
        if(voices < 1)
            voices = 1;
    }

    /**
     * Give your score a name.
     * 
     * @param name A String, the title of your score
     */
    public void nameScore(String name) {
        this.name = name;
    }

    /**
     * Adjusts iTime for given Note object's duration.
     * 
     * @param note Note object
     */
    public void setNoteITime(Note note) {
        note.iTime = this.iTime;
        this.iTime += note.rhythmicValue;
    }

    /**
     * Adjusts iTime for given array of Note objects' durations.
     * 
     * @param notes Array of Notes
     */
    public void setNoteITimes(Note[] notes) {
        for(int i = 0; i < notes.length; ++i) {
            setNoteITime(notes[i]);
        }
    }

    /**
     * Adjusts iTime for given array of Chords' durations.
     */
    public void setChordITimes(Chord[] chords) {
        for(int i = 0; i < chords.length; ++i) {
            for(int j = 0; j < chords[i].notes.length; ++j) {
                chords[i].notes[j].iTime = this.iTime;
            }
            this.iTime += chords[i].notes[0].rhythmicValue;
        }
    }

    /**
     * Returns the current number of voices in the score.
     * 
     * @return Current number of voices in score
     */
    public int numVoices() {
        return voices;
    }

    /**
     * Sets the bpm to a given integer
     * 
     * @bpm An integer
     */
    public void setBPM(int bpm) {
        this.bpm = bpm;
    }

    /**
     * Sets iTime to zero and sets variable extantVoices to previous iTime value.
     */
    public void resetITime() {
        iTime = 0;
        extantVoices = voices;
    }

    /**
     * Saves score as a .csd file in the Compositions folder.
     */
    public void toCSD() {
        String dotCSDFile = "<CsoundSynthesizer>\n<CsOptions>\n\n-odac\n\n" +
                            "</CsOptions>\n<CsInstruments>\n\nsr = 44100\n" +
                            "ksmps = 2\nnchnls = 2\n0dbfs = 4\n\n";
        for(int i = 1; i <= (voices + extantVoices); ++i) {
            Note note = score.get(i);
            dotCSDFile += "instr " + i + 
                          "\n\nares linen .5, 0.1, p3, .01\n" +
                          "asig oscil ares, cpspch(p4)\n     outs asig,asig" +
                          "\n\nendin\n\n";
        }
        dotCSDFile += "</CsInstruments>\n<CsScore>\n\nt 0 " + bpm + "\n\n";
        for(int i = 0; i < score.size(); ++i) {
            Note note = score.get(i);
            dotCSDFile += "i " + note.voice + " " + note.iTime + " " + 
                          note.rhythmicValue + " " + note.pitchToPCH() 
                          + "\n";
        }
        dotCSDFile += "\ne\n</CsScore>\n</CsoundSynthesizer>";
        byte[] data = dotCSDFile.getBytes();
        OutputStream output = null;
        try {
            output = new BufferedOutputStream(Files.newOutputStream(getPath(), 
                                                                    CREATE));
            output.write(data);
            output.flush();
            output.close();
        }
        catch(Exception e) {
            System.out.println(e);
        }
    }
    
    /**
     * Calls cSound on score's .csd file and plays numPlays number of times.
     * 
     * @param numPlays an integer, number of times to repeat playing score
     */
    public void play(int numPlays) throws InterruptedException,IOException {
        for(int i = 0; i < numPlays; ++i) {
            Path path = getPath();
            Process process = new ProcessBuilder("/usr/local/bin/csound", // change if different
                                                 path.toString()).start();
            process.waitFor();
        }
    }
    
    /**
     * Creates and returns a new path for the score, placing it in the 
     * Compositions folder and tagging a .csd suffix.
     * 
     * @return Path of a score's .csd file
     */
    public Path getPath() {
        Path inputPath = Paths.get("Compositions/" + name + ".csd");
        Path fullPath = inputPath.toAbsolutePath();
        return fullPath;
    }
}
