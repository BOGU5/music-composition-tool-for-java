
/**
 * A virtual musical scale and some accompanying methods.
 *
 * @author Zach McDaniel
 * @version 2019/5/26
 */
class Scale extends Note {
    Note key;
    Note[] scale;
    double rhythmicValue;
    int[] intervals; 
    String name;
    /**
     * Construct a scale with given Note object as tonal center,
     * an array of integers representing the notes of the scale
     * (integers representing number of semi-tones from tonal center),
     * and a rhythmic value.
     * 
     * @param key Note object
     * @param intervals Array of integers representing semi-tones from root
     * @param rhythmicValue double the rate at which the scale is played
     */
    public Scale(Note key, int[] intervals, double rhythmicValue) {
        this(key, intervals);
        this.rhythmicValue = rhythmicValue;
    }

    /**
     * Construct a scale with tonal center and intervals only, omitting 
     * any rhythmic values.
     * 
     * @param key Note object
     * @param intervals Array of integers representing semi-tones from root
     */
    public Scale(Note key, int[] intervals) {
        this(intervals);
        for(int i = 0; i < intervals.length; ++i) {
            scale[i] = key.nSemiTonesFrom(intervals[i]);
            scale[i].setRhythmicValue(rhythmicValue);
        }
        this.key = key;
    }

    /**
     * Construct a scale, and give it a name.
     * 
     * @param name String object
     * @param key Note object
     * @param intervals Array of integers representing semi-tones from root
     * @param rhythmicValue double
     */
    public Scale(String name, Note key, 
    int[] intervals, double rhythmicValue) {
        this(key, intervals, rhythmicValue);
        this.name = name;
    }

    /**
     * Construct a scale skeleton with only interval numbers.
     * 
     * @param intervals Array of integers representing semi-tones from root
     */
    public Scale(int[] intervals) {
        scale = new Note[intervals.length];
        this.intervals = intervals;
    }
    
    /**
     * Display the value, octave, iTime and duration of each note in scale.
     */
    public void displayScaleDetails() {
        for(int i = 0; i < scale.length; ++i) {
            scale[i].displayNoteDetails();
        }
    }
    
    /**
     * Display the letter names of all notes in scale. Inserts a new line
     * every four notes and a comma between each note.
     * 
     * @return String of all notes' letter values
     */
    public String noteNames() {
        String string = "";
        for(int i = 0; i < scale.length; ++i) {
            if (i == scale.length - 1)
                string += scale[i].getLetterValue();
            else if ((i + 1) % 4 == 0)
                string += scale[i].getLetterValue() + ",\n";
            else
                string += scale[i].getLetterValue() + ", ";
        }
        return string;
    }

    /**
     * Gives the abbreviation for each interval represented in scale.
     * Inserts a new line every four intervals and a comma between each 
     * interval.
     * 
     * @return String of all interval names
     */
    public String intervalNames() {
        String intervalNames = "";
        for(int i = 0; i < intervals.length; ++i) {
            if (i == intervals.length - 1)
                intervalNames += super.intervalName(intervals[i]);
            else if ((i + 1) % 4 == 0)
                intervalNames += super.intervalName(intervals[i]) + ",\n";
            else
                intervalNames += super.intervalName(intervals[i]) + ", ";   
        }
        return intervalNames;
    }
}
