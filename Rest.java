
/**
 * A virtual musical rest.
 *
 * @author Zach McDaniel
 * @version 2019/5/26
 */
public class Rest extends Note {
    public Rest(double rtm) {
        rhythmicValue = rtm;
    }
}