
/**
 * A virtual musical melody.
 *
 * @author Zach McDaniel
 * @version 2019/5/26
 */
import java.util.ArrayList;
import java.util.List;
class Melody extends Note {
    List<Note> notes = new ArrayList<>();
    /**
     * Default constructor for initialization.
     */
    public Melody() {
    }
    
    /**
     * Add a Note object to the melody.
     * 
     * @param note A Note object
     */
    public void addNote(Note note) {
        notes.add(note);
    }
    
    /**
     * Add a Scale object to the melody.
     * 
     * @param scale A Scale object
     */
    public void addScale(Scale scale) {
        Note[] notes = scale.scale;
        for(int i = 0; i < notes.length; ++i) {
            this.notes.add(notes[i]);
        }
    }

    /**
     * Add an array of notes to the melody.
     * 
     * @param notes An array of Note objects
     */
    public void addNotes(Note[] notes) {
        for(int i = 0; i < notes.length; ++i) {
            this.notes.add(notes[i]);
        }
    }
}
