
/**
 * Write a description of class NoteTest here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class NoteTest {
    public static void main(String args[]){
        Note note1 = new Note();
        Note note2 = new Note(4, 3, 0.5);
        Note note3 = new Note(29, 0.5);
        note1.displayNoteDetails();
        printNewLine();
        note2.displayNoteDetails();
        printNewLine();
        note3.displayNoteDetails();
        printNewLine();
        int num = 4;
        System.out.println("The note " + num + " semi-tones from " +
            note1.getLetterValue() + " is " +
            note1.nSemiTonesFrom(num).getLetterValue());
        printNewLine();
        note1.nSemiTonesFrom(num).displayNoteDetails();
        //int[] majorScale = {0, 2, 4, 5, 7, 9, 11};
        printNewLine();
        Note note4 = new Note();
        note4 = note4.nSemiTonesFrom(9);
        Scale scale = note4.naturalMinorScale(0.5);
        for(int i = 0; i < scale.scale.length; ++i) {
            System.out.println("Details of Note number " + (i + 1) +
                " from "+ scale.name);
            scale.scale[i].displayNoteDetails();
        }
        System.out.println("\n" + scale.noteNames());
        printNewLine();
        System.out.println(scale.intervalNames());
        printNewLine();
        Note d = new Note(6, 4, 1.0); // D, 4th octave, quarter note
        int[] majorProgressionRelations = new int[] {0, 7, 9, 4, 5, 0, 5, 7};
        Progression chordProgression = new Progression(d, "major",
                                                       majorProgressionRelations);
        for(int i = 0; i < chordProgression.progression.length; ++i) {
            System.out.println(chordProgression.progression[i].name);
        }
        for(int i = 0; i < chordProgression.progression.length; ++i) {
            for(int j = 0; j < chordProgression.progression[i].notes.length; ++j) {
                chordProgression.progression[i].notes[j].setRhythmicValue(4.0);
            }
        }
        printNewLine();
        Chord chord = note1.minorTriad(2);
        System.out.println(chord.name);
        for(int i = 0; i < chord.notes.length; ++i) {
            System.out.println("Details of Note number " + (i + 1) +
                " from "+ chord.name);
            chord.notes[i].displayNoteDetails();
        }
        Score canon = new Score();
        canon.addProgression(chordProgression);
        canon.setBPM(100);
        canon.toCSD();
    }

    public static void printNewLine() {
        System.out.println("");
    }
}
