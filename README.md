simple music composition tool for java, to utilize the play() method, download cSound @ https://csound.com

A Tool For Music Composition

	A package of classes and methods in the Java language designed for music composition. 
	Classes include: Note (a set of methods for creating and manipulating notes), 
	Scale(a set of methods for creating Arrays of Note objects in succession), 
	Chord (a set of methods for creating and manipulating arrays of Note objects played in unison), 
	Progression (a set of methods for creating and manipulating arrays of Chord objects), 
	Melody (a set of methods for creating and manipulating an ArrayList of Note objects and Scale objects, 
	to be added to and manipulated at one’s leisure), 
	and Score (a set of methods for creating and manipulating an ArrayList of Note objects and Note’s subclasses — 
	all of the above — and organizing the various elements so as to create a piece of music).
	The program is still rather simple and limited, but I’ve tried to write it in a way that doesn’t 
	explicitly dictate how it should be used and allows for extension and experimentation.


Introduction to the Program

	Everything in the program is basically centered toward the Note object, which contains most of the methods 
	and from which all other classes extend. Note objects can be constructed without providing all the information 
	they can hold, but all in all the variables a  Note object keeps track of are: 
 ```
int pitch; // An integer 1-12 representing A through A-Flat/G-Sharp
 int octave; // Octave of a note, without any corresponding reality
             // this can be really any number, serves to show       
             // relations between notes
 double rhythmicValue; // The duration of a note, 1.0 could be a quarter note for example, 0.5 an eighth note
 double iTime; // The time (beat) at which a Note will begin in the score
 String letterValue; // The letter name of the Note
```
A note might be created like this:
 ```
 Note c = new Note(4, 4, 1.0); // a C/B-Sharp quarter note in the fourth octave
 ```
 Now to create a c major triad and its first and second inversion:
```
Chord cMajorTriad = c.majorTriad();
Chord cMajorTriadFirstInversion = c.majorTriad(1);
Chord cMajorTriadSecondInversion = c.majorTriad(2);
```
These new chord objects can also be arpeggiated simply:
```
Melody cMajorArpeggio = cMajorTriad.chordArpeggiator(1.0);
```
The above statement creates a melody object with three quarter notes, the arpeggiated C major triad.
There are also built in methods for basic scales:
```
Scale cHarmonicMinor = c.harmonicMinorScale(1.0);
	// Creates a Scale object, the C harmonic minor scale, an array of quarter notes of each note in the scale in succession.
```	
Chord progressions are also simply built. Here is Pachelbel’s canon:
```
Note d = new Note(5, 4, 1.0); // D, 4th octave, quarter note
int[] majorProgressionRelations = new int[] {0, 7, 9, 4, 5, 0, 5, 7};
Progression chordProgression = new Progression(d, "major", majorProgressionRelations);
```
To see the details of each triad in the progression:
```
for(int i = 0; i < chordProgression.progression.length; ++i) {
     System.out.println(chordProgression.progression[i].name);
}
```
This prints:
```
D Major Triad
A Major Triad
B/C-Flat Minor Triad
F-Sharp/G-Flat Minor Triad
G Major Triad
D Major Triad
G Major Triad
A Major Triad
```

